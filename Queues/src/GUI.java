import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;

import javax.swing.*;

public class GUI {
	CardLayout cl = new CardLayout();
	JFrame frame = new JFrame("Queues Simulation");
	JPanel container = new JPanel();
	JPanel panel = new JPanel();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JPanel panel5 = new JPanel();
	JPanel panel6 = new JPanel();
	JPanel panel7 = new JPanel();
	JPanel simPanel = new JPanel();
	JPanel qPanel = new JPanel();
	JPanel logPanel = new JPanel();

	JTextField nrQueues = new JTextField(3);
	JTextField minArrInterval = new JTextField(3);
	JTextField maxArrInterval = new JTextField(3);
	JTextField minServTime = new JTextField(3);
	JTextField maxServTime = new JTextField(3);
	JButton startBut = new JButton("Start simulation");

	JLabel simStartlbl = new JLabel("Simulation Time: ");
	JLabel simEndlbl = new JLabel(" to ");
	JTextField startHr = new JTextField(2);
	JTextField startMin = new JTextField(2);
	JTextField endHr = new JTextField(2);
	JTextField endMin = new JTextField(2);

	JLabel qLbl = new JLabel("Number of queues: ");
	JLabel miArrLbl = new JLabel("Minimum arrival interval: ");
	JLabel maArrLbl = new JLabel("Maximum arrival interval: ");
	JLabel miServLbl = new JLabel("Minimum service time: ");
	JLabel maServLbl = new JLabel("Maximum service time: ");
	JLabel pts1 = new JLabel(":");
	JLabel pts2 = new JLabel(":");

	JButton log = new JButton("Open log");
	JButton retBut = new JButton("Abort");
	public JTextField ct;
	public JTextArea[] qScreens;
	Handler simHandler;

	public GUI() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setVisible(true);

		frame.add(container);
		container.setLayout(cl);
		container.add(panel, "1");
		container.add(simPanel, "2");

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(panel1);
		panel.add(panel2);
		panel.add(panel3);
		panel.add(panel4);
		panel.add(panel5);
		panel.add(panel6);
		panel.add(panel7);

		simPanel.setLayout(new BoxLayout(simPanel, BoxLayout.Y_AXIS));
		simPanel.add(qPanel);
		ct = new JTextField(8);
		qPanel.add(ct);
		simPanel.add(logPanel);

		panel1.add(qLbl);
		panel1.add(nrQueues);
		panel2.add(miArrLbl);
		panel2.add(minArrInterval);
		panel3.add(maArrLbl);
		panel3.add(maxArrInterval);
		panel4.add(miServLbl);
		panel4.add(minServTime);
		panel5.add(maServLbl);
		panel5.add(maxServTime);
		panel6.add(simStartlbl);
		panel6.add(startHr);
		panel6.add(pts1);
		panel6.add(startMin);
		panel6.add(simEndlbl);
		panel6.add(endHr);
		panel6.add(pts2);
		panel6.add(endMin);
		panel7.add(startBut);
		panel.revalidate();

		logPanel.add(log);
		logPanel.add(retBut);
		logPanel.revalidate();
		
		startBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (nrQueues.getText().equals("") || minArrInterval.getText().equals("")
						|| maxArrInterval.getText().equals("") || minServTime.getText().equals("")
						|| maxServTime.getText().equals("") || startHr.getText().equals("")
						|| startMin.getText().equals("") || endHr.getText().equals("") || endMin.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please complete all fields!", "Error", 0);

				} else if (Integer.parseInt(nrQueues.getText()) < 1 || Integer.parseInt(minArrInterval.getText()) < 1
						|| Integer.parseInt(maxArrInterval.getText()) < 1 || Integer.parseInt(minServTime.getText()) < 1
						|| Integer.parseInt(maxServTime.getText()) < 1 || Integer.parseInt(startHr.getText()) < 0
						|| Integer.parseInt(startMin.getText()) < 0 || Integer.parseInt(endHr.getText()) < 0
						|| Integer.parseInt(endMin.getText()) < 0 || Integer.parseInt(startHr.getText()) > 23 || Integer.parseInt(endHr.getText()) > 23
						|| Integer.parseInt(startMin.getText()) > 59 || Integer.parseInt(endMin.getText()) > 59){
					JOptionPane.showMessageDialog(null,
							"Invalid input", "Error", 0);

				} else if (Integer.parseInt(maxArrInterval.getText()) < Integer.parseInt(minArrInterval.getText())
						|| Integer.parseInt(maxServTime.getText()) < Integer.parseInt(minServTime.getText())) {
					JOptionPane.showMessageDialog(null, "Invalid time bounds!", "Error", 0);

				} else if (Integer.parseInt(startHr.getText()) == Integer.parseInt(endHr.getText())
						&& Integer.parseInt(startMin.getText()) >= Integer.parseInt(endMin.getText())) {
					JOptionPane.showMessageDialog(null, "Invalid sim interval!", "Error", 0);

				} else if (Integer.parseInt(startHr.getText()) > Integer.parseInt(endHr.getText())) {
					JOptionPane.showMessageDialog(null, "Invalid sim interval!", "Error", 0);
				} else {
					int nrQ = Integer.parseInt(nrQueues.getText());
					int maxArr = Integer.parseInt(maxArrInterval.getText());
					int minArr = Integer.parseInt(minArrInterval.getText());
					int maxSer = Integer.parseInt(maxServTime.getText());
					int minSer = Integer.parseInt(minServTime.getText());
					LocalTime start = LocalTime.of(Integer.parseInt(startHr.getText()),
							Integer.parseInt(startMin.getText()));
					LocalTime stop = LocalTime.of(Integer.parseInt(endHr.getText()),
							Integer.parseInt(endMin.getText()));

					qScreens = new JTextArea[nrQ];

					for (int i = 0; i < nrQ; i++) {
						qScreens[i] = new JTextArea(20, 10);
						qPanel.add(qScreens[i]);
						qScreens[i].setEditable(false);
						qScreens[i].setBorder(
								BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK),
										BorderFactory.createEmptyBorder(10, 10, 10, 10)));
					}
					qPanel.revalidate();
					cl.show(container, "2");
					frame.pack();
					
					simHandler = new Handler(nrQ, maxArr, minArr, maxSer, minSer, start, stop, qScreens, ct);
					simHandler.execute();
					
				}
			}
		});

		log.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					File fp = new File("Log.txt");
					Desktop.getDesktop().open(fp);
				} catch (IOException ioEx) {
					JOptionPane.showMessageDialog(null, "Error opening log file", "Error", 0);
				}
			}
		});
		
		retBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				simHandler.cancel(true);
				simHandler.setCurrentTime(simHandler.getStopTime());
				cl.show(container, "1");
				for(int i = 0; i < qScreens.length; i++) {
					qPanel.remove(qScreens[i]);
				}
				frame.setSize(400, 400);
			}
		});
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new GUI();
			}
		});
	}
}
