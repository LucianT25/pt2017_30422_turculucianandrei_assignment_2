
/**
 * The handler class manages the simulation.
 * It extends the SwingWorker class so it can run in parallel with the 
 * GUI and update it periodically.
 * An array of Queues is generated, depending on the nrOfQueues variable,
 * which is set by the user.
 * Whenever it is the time, the handler spawns a new client and sends
 * him to the smallest queue.
 */
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalTime;

public class Handler extends SwingWorker<Void, String> {
	private int nrOfQueues;
	private int maximumArrivalInterval; //time between customer arrivals
	private int minimumArrivalInterval;
	private int maximumServiceTime; //time required to serve a customer
	private int minimumServiceTime;
	private LocalTime simulationStart; //simulation time boundaries
	private LocalTime simulationStop;
	private Queue[] queues; //array of queues
	private LocalTime currentTime; //simulation time
	private int arrivalInterval; //random arrival interval
	private int serviceTime; //random service time
	private JTextArea[] qScreens;
	private JTextField timer;
	private volatile int clientsServed; //number of clients served so far; volatile because multiple threads can access it at the same time
	private PrintWriter pw; //printed created and passed to the queues through their constructor
	private LocalTime peak; //peak hour
	private int currentClientNr;
	private int maxClients; //maximum total of customers in all queues

	public Handler(int nrQ, int maxArr, int minArr, int maxSer, int minSer, LocalTime start, LocalTime stop,
			JTextArea[] qS, JTextField t) {
		this.nrOfQueues = nrQ;
		this.maximumArrivalInterval = maxArr;
		this.minimumArrivalInterval = minArr;
		this.maximumServiceTime = maxSer;
		this.minimumServiceTime = minSer;
		this.simulationStart = start;
		this.simulationStop = stop;
		this.queues = new Queue[nrOfQueues];
		this.currentTime = this.simulationStart;
		this.arrivalInterval = 0;
		this.serviceTime = 0;
		this.qScreens = qS;
		this.timer = t;
		this.clientsServed = 0;
		this.currentClientNr = 0;
		this.maxClients = 0;
	}

	public void refreshValues(int nrQ, int maxArr, int minArr, int maxSer, int minSer, LocalTime start, LocalTime stop,
			JTextArea[] qS, JTextField t) {
		this.nrOfQueues = nrQ;
		this.maximumArrivalInterval = maxArr;
		this.minimumArrivalInterval = minArr;
		this.maximumServiceTime = maxSer;
		this.minimumServiceTime = minSer;
		this.simulationStart = start;
		this.simulationStop = stop;
		this.qScreens = qS;
		this.timer = t;
	}

	public LocalTime getCurrentTime() {
		return this.currentTime;
	}
	
	public void setCurrentTime(LocalTime lt) {
		this.currentTime = lt;
	}
	
	public LocalTime getStopTime() {
		return this.simulationStop;
	}

	public int getNrOfQueues() {
		return this.nrOfQueues;
	}

	public Queue[] getQueues() {
		return this.queues;
	}

	public void incClientsServed() {
		this.clientsServed++;
	}

	public int getClientsServed() {
		return this.clientsServed;
	}

	public int sendCustomerToSmallestQueue(Customer c) {
		int minSize = Integer.MAX_VALUE;
		int minIndex = Integer.MAX_VALUE;

		for (int j = 0; j < queues.length; j++) {
			if (queues[j].getCustomers().size() < minSize && queues[j].isFull() == false) {
				minIndex = j;
				minSize = queues[j].getCustomers().size();
			}
		}

		queues[minIndex].enqueue(c);
		return minIndex;
	}

	@Override
	protected Void doInBackground() throws Exception {

		try {
			pw = new PrintWriter("Log.txt");
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "File not found!", "Error", 0);
		}
		for (int j = 1; j <= nrOfQueues; j++) {
			this.queues[j - 1] = new Queue(true, "Queue " + j, this, pw);
			queues[j - 1].start();
			// System.out.println("Spawning " + "Queue "+j);
		}

		Random random = new Random();
		arrivalInterval = random.nextInt(this.maximumArrivalInterval + 1 - this.minimumArrivalInterval)
				+ this.minimumArrivalInterval;
		int i = 1;

		while (this.currentTime.compareTo(this.simulationStop) <= 0) {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("Thread interrupted");
			}

			this.currentTime = this.currentTime.plusMinutes(1);
			publish(currentTime.toString());

			if (currentTime.compareTo(simulationStart.plusMinutes(arrivalInterval)) >= 0) {
				serviceTime = random.nextInt(this.maximumServiceTime + 1 - this.minimumServiceTime)
						+ this.minimumServiceTime;
				Customer c = new Customer(i, this.currentTime, this.serviceTime);
				sendCustomerToSmallestQueue(c);
				i++;

				simulationStart = simulationStart.plusMinutes(arrivalInterval);
				arrivalInterval = random.nextInt(this.maximumArrivalInterval + 1 - this.minimumArrivalInterval)
						+ this.minimumArrivalInterval;
			}

			this.currentClientNr = 0;
			for (Queue q : this.queues) {
				publish(q.toString());
				this.currentClientNr += q.getCustomers().size();
			}
			if (currentClientNr > maxClients) {
				maxClients = currentClientNr;
				peak = LocalTime.of(currentTime.getHour(), currentTime.getMinute());
			}
		}

		return null;
	}

	@Override
	protected void process(final List<String> chunks) {
		timer.setText(chunks.get(0));
		for (int i = 1; i < chunks.size(); i++) {
			qScreens[i - 1].setText(chunks.get(i));
		}
		chunks.clear();
	}

	@Override
	protected void done() {
		pw.append(this.clientsServed + " clients have been served." + "\r\n" + "Peak hour: " + peak.toString() + " - "
				+ maxClients + " in queues.");
		pw.close();
		JOptionPane.showMessageDialog(null, "Simulation has ended - " + getClientsServed() + " clients served.",
				"Job done!", JOptionPane.INFORMATION_MESSAGE);
	}
}
